<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    protected $fillable=['name','available'];
    public function products(){

        return $this->hasmany(Product::class);


    }
}
