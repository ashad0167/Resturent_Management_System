<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('user.index');
});
Route::get('/services', function () {
    return view('user.services');
});
Route::get('/about', function () {
    return view('user.about');
});
Route::get('/gallery', function () {
    return view('user.gallery');
});
Route::get('/codes', function () {
    return view('user.codes');
});
Route::get('/contact', function () {
    return view('user.contact');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix'=>'admin','middleware'=>'auth'], function () {
    Route::get('/',function (){
    return view('admin.index');

})->name('admin.index');

});

Route::get('/product', function () {
    return view('admin.product.create');
});


Route::get('/logout', 'Auth\LoginController@logout');