
        <!DOCTYPE HTML>
<html>
<head>
    <title>Admin Panel</title>
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="Modern Responsive web template, Bootstrap Web Templates, Flat Web Templates, Andriod Compatible web template,
Smartphone Compatible web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <!-- Bootstrap Core CSS -->
    <link href="admincss/bootstrap.min.css" rel='stylesheet' type='text/css' />
    <!-- Custom CSS -->
    <link href="admincss/style.css" rel='stylesheet' type='text/css' />
    <link href="admincss/font-awesome.css" rel="stylesheet">
    <!-- jQuery -->
    <script src="adminjs/jquery.min.js"></script>
    <!----webfonts--->
    <link href='http://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
    <!---//webfonts--->
    <!-- Bootstrap Core JavaScript -->
    <script src="adminjs/bootstrap.min.js"></script>
    <style>
        h1{
            color: crimson;
        }

        </style>
</head>
<body id="login">
<br><br><br>
<div class="login-logo">
    <center><h1>ADMIN</h1></center>
</div>
<h2 class="form-heading">login</h2>
<div class="app-cam">
    <form method="POST" action="{{ route('login') }}">
        {{ csrf_field() }}
        <input type="text" class="text" value="E-mail address" name="email">
        <input type="password" value="Password" name="password" >
        <div class="submit"><input type="submit" onclick="myFunction()" value="Login"></div>
        <div class="login-social-link">
            <a href="https://www.facebook.com/asadzaman0167" class="facebook">
                Facebook
            </a>
            <a href="https://www.facebook.com/asadzaman0167" class="twitter">
                Twitter
            </a>
        </div>
        <ul class="new">
            <li class="new_left"><p><a href="#">Forgot Password ?</a></p></li>
            <li class="new_right"><p class="sign">New here ?<a href="/register"> Sign Up</a></p></li>
            <div class="clearfix"></div>
        </ul>
    </form>
</div>
<div class="copy_layout login">
    <p>Copyright &copy; 2017 Modern. All Rights Reserved | Design by <a href="https://www.facebook.com/asadzaman0167" target="_blank">asadzaman</a> </p>
</div>
</body>
</html>





